'''
Author       : Deng
Date         : 2024-04-18 16:05:20
LastEditors  : Joel
LastEditTime : 2024-04-18 17:46:12
FilePath     : \jlink_hc32f460_programer.py
Description  : 
Copyright 2024 YiJiaLink, All Rights Reserved. 
2024-04-18 16:05:20
'''

import os
cmd = 'JFlash.exe -openprjmcu_setting/HC32_jlink.jflash -connect -openbuild/hc32f460_temp.hex -erasechip -programverify -startapplication -exit'
res = os.popen(cmd)
output_str = res.read()   # 获得输出字符串
print(output_str)
