/*
 * @Author       : Deng
 * @Date         : 2024-04-26 10:08:07
 * @LastEditors  : Joel
 * @LastEditTime : 2024-05-19 11:43:07
 * @FilePath     : \Include\ws2812b.h
 * @Description  : 
 * Copyright 2024 YiJiaLink, All Rights Reserved. 
 * 2024-04-26 10:08:07
 */
#ifndef __WS2812B_H
#define __WS2812B_H

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include "hc32_ll.h"
#include <stdio.h>
#include "rgb.h"
#include "linked_list.h"
#if defined(__cplusplus)
extern "C"
{
#endif /*_cplusplus*/

#define RGB_BIT 24
#define LED_QTY (100)

    typedef enum _FLASH
    {
        LED_FLOW,         // 单色递增递减
        LINK_FLOW,        // 链表循环
        GRAD_COLOR,       // 自定义彩色呼吸
        BRIGHTNESS,       // 音乐
        FLASH_RGB,        // 闪烁
        CUSTOMER_SETTING, // 客户自定义
        OFF_RGB,          // 关闭
        ON_RGB            // 打开
    } FLASH_MODE;
#define LINK_INIT_LEN 20
    /* 公共(可被其他源文件使用)宏、枚举、结构体的定义 */
    typedef struct _init_rgb_set
    {
        uint8_t color_flag;
        uint32_t color_set[LINK_INIT_LEN];
        uint32_t link_rgb_data[LINK_INIT_LEN];
        uint32_t color[LED_QTY];
        uint32_t led_index, move_index,link_index;
        uint8_t bit24;
        FLASH_MODE LED_MODE;
        float brightness;

        uint8_t brightness_index;
        uint8_t red_index;
        uint8_t green_index;
        uint8_t blue_index;
        uint16_t index_qty;
        uint8_t color_index;
        uint16_t link_len;

    } init_rgb_set;
    extern init_rgb_set rgb_set;

    void init_ws2812b(void);
    void run_ws2812b(void);
    void inint_mode(FLASH_MODE LED_MODE);
    uint32_t color_uint8_uint32(uint8_t red,uint8_t green,uint8_t blue);
    uint32_t change_level(uint32_t rgb, float k);
    link *set_link_brightness_init(uint32_t data[], uint32_t length);

#if defined(__cplusplus)
}
#endif /*_cplusplus*/

#endif
/* el psy congroo */
