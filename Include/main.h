/*
 * @Author       : Deng
 * @Date         : 2024-04-26 10:08:07
 * @LastEditors  : Joel
 * @LastEditTime : 2024-05-19 09:27:24
 * @FilePath     : \Include\main.h
 * @Description  : 
 * Copyright 2024 YiJiaLink, All Rights Reserved. 
 * 2024-04-26 10:08:07
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#include "hc32_ll.h"
#include <stdio.h>
#include "system_hc32f460.h"
#include "usr_system.h"
#include "usr_timer.h"
#include "ws2812b.h"
#include "rgb.h"
#include "linked_list.h"
#endif /* __MAIN_H__ */

/*******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/
