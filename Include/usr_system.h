/*
 * @Author       : Deng
 * @Date         : 2024-04-18 20:32:50
 * @LastEditors  : Joel
 * @LastEditTime : 2024-04-18 20:32:55
 * @FilePath     : \Include\usr_system.h
 * @Description  : 
 * Copyright 2024 YiJiaLink, All Rights Reserved. 
 * 2024-04-18 20:32:50
 */
/*
 * @Author: Joel
 * @Date: 2023-05-17 20:01:27
 * @LastEditors: Joel
 * @LastEditTime: 2023-05-19 17:10:38
 * @FilePath: /hc32f460_freertos/usr_app/inc/usr_system.h
 * @Description: 
 * Copyright (c) 2022 by DongGuan City YiJiaLink, All Rights Reserved. 
 */
#ifndef __USR_SYSTEM_H__
#define __USR_SYSTEM_H__
#include "hc32_ll.h"
/**
 * @defgroup BSP_XTAL_CONFIG BSP XTAL Configure definition
 * @{
 */
#define BSP_XTAL_PORT                   (GPIO_PORT_H)
#define BSP_XTAL_IN_PIN                 (GPIO_PIN_01)
#define BSP_XTAL_OUT_PIN                (GPIO_PIN_00)

#define BSP_XTAL32_PORT                 (GPIO_PORT_C)
#define BSP_XTAL32_IN_PIN               (GPIO_PIN_15)
#define BSP_XTAL32_OUT_PIN              (GPIO_PIN_14)



void user_system_init(void);

#endif 