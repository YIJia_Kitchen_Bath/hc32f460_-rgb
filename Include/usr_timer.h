/*
 * @Author       : Deng
 * @Date         : 2024-04-18 22:25:42
 * @LastEditors  : Joel
 * @LastEditTime : 2024-05-17 12:11:44
 * @FilePath     : \Include\usr_timer.h
 * @Description  :
 * Copyright 2024 YiJiaLink, All Rights Reserved.
 * 2024-04-18 22:25:42
 */
/*
 * @Author       : Deng
 * @Date         : 2024-04-18 22:25:42
 * @LastEditors  : Joel
 * @LastEditTime : 2024-04-19 22:44:40
 * @FilePath     : \Include\usr_timer.h
 * @Description  :
 * Copyright 2024 YiJiaLink, All Rights Reserved.
 * 2024-04-18 22:25:42
 */

#ifndef __USER_TIMER_H
#define __USER_TIMER_H

/*******************************************************************************
 * Include files
 ******************************************************************************/
#include "hc32_ll.h"

#if defined(__cplusplus)
extern "C"
{
#endif /*_cplusplus*/

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define RED_SET_PORT 0x40053818
#define RED_RESET_PORT 0x4005381A
#define RED_TOGGLE_PORT 0x4005381C
#define GREEN_GPIO_PIN 0x8000
#define RED_GPIO_PIN 0x4000
#define BLUE_GPIO_PIN 0x2000
#define WHITE_GPIO_PIN 0x1000
#define PWM_TIMER_CLR 0x40024414
#define PWM_CYCLE 255
    /* 公共(可被其他源文件使用)宏、枚举、结构体的定义 */
    typedef struct _init_rgb_pwm
    {
        uint8_t brightness_index;
        uint16_t red_duty;
        uint16_t green_duty;
        uint16_t blue_duty;
        uint16_t white_duty;

        uint16_t index_red;
        uint16_t index_green;
        uint16_t index_blue;
        uint16_t index_white;

        uint16_t time;
        uint8_t flag;
        uint8_t data;

        uint8_t customer_level_r;
        uint8_t customer_level_g;
        uint8_t customer_level_b;

    } init_rgb_pwm;
    /* 外部全局变量的声明 */
    extern init_rgb_pwm set_rgb_pwm;
    extern init_rgb_pwm *p_pwm;

    typedef enum _FLASH_RGBW
    {
        RED,   // 红色
        GREEN, // 绿色
        BLUE,  // 蓝色
        WHITE  // 白色
    } FLASH_RGBW;
/* TMR0 unit and channel definition */
#define TMR0_UNIT (CM_TMR0_1)
#define TMR0_CLK (FCG2_PERIPH_TMR0_1)
#define TMR0_CH (TMR0_CH_B)
#define TMR0_TRIG_CH (AOS_TMR0)
#define TMR0_CH_INT (TMR0_INT_CMP_B)
#define TMR0_CH_FLAG (TMR0_FLAG_CMP_B)
#define TMR0_INT_SRC (INT_SRC_TMR0_1_CMP_B)
#define TMR0_IRQn (INT007_IRQn)

/* Period = 1 / (Clock freq / div) * (Compare value + 1) = 500ms */
#define TMR0_CMP_VALUE (246 * 1)

    void TMR0_Config(void);
    /*******************************************************************************
     * API
     ******************************************************************************/

    /* 外部函数(可加 extern 修饰)的声明 */
    void User_Timer_Init(void);
    /* 定时器中断 */
    void User_Timer_IRQHandler(void);

#if defined(__cplusplus)
}
#endif /*_cplusplus*/

#endif
/* el psy congroo */
