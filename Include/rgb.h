/*
 * @Author       : Deng
 * @Date         : 2024-04-26 17:53:50
 * @LastEditors  : Joel
 * @LastEditTime : 2024-05-17 12:20:13
 * @FilePath     : \src\rgb.h
 * @Description  :
 * Copyright 2024 YiJiaLink, All Rights Reserved.
 * 2024-04-26 17:53:50
 */
#ifndef RGB_H
#define RGB_H

#include "ws2812b.h"
#include "usr_timer.h"
#include <math.h>

int run_rgbw(void);
#define PI 3.14159265
float get_apm_value(uint16_t time, uint16_t a,uint16_t b,uint8_t flag);
void rgb_math_run(init_rgb_pwm *p_pwm);

#endif
