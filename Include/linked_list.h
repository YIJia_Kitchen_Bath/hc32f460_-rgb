/*
 * @Author       : Joel
 * @Date         : 2023-08-24 11:55:06
 * @LastEditors  : Joel
 * @LastEditTime : 2024-05-19 09:35:41
 * @FilePath     : \Include\linked_list.h
 * @Description  : 
 * Copyright 2023 Siliten, All Rights Reserved. 
 * 2023-08-24 11:55:06
 */

#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__

//  header
#include "hc32f460.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* rgbw 4字节链表 */
typedef struct Link{
     uint32_t  elem;
    struct Link *next;
}link,*NodeList;
/* 初始化链表 */
link * initLink(uint32_t data[],uint32_t length);
//链表插入的函数，p是链表，elem是插入的结点的数据域，add是插入的位置
link * insertElem(link * p,uint32_t elem,int add);
//删除结点的函数，p代表操作链表，add代表删除节点的位置
link * delElem(link * p,int add);
//查找结点的函数，elem为目标结点的数据域的值
int selectElem(link * p,uint32_t elem);
//更新结点的函数，newElem为新的数据域的值
link *amendElem(link * p,int add,int newElem);
int display_lenth(link *p);
int display_last_data(link *p);
link * disposal_LinkList(link *headNode);
link * insert_list_last(link *head, uint8_t data);
uint32_t list_conver_to_arr(link *node, uint32_t data_arr[], uint16_t length);
/* 测试链表 */
int linklist_test_main(void);

#ifdef __cplusplus
}
#endif

#endif