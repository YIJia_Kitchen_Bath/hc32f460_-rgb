/*
 * @Author       : Deng
 * @Date         : 2024-04-18 19:48:18
 * @LastEditors  : Joel
 * @LastEditTime : 2024-05-17 21:28:03
 * @FilePath     : \src\main.c
 * @Description  :
 * Copyright 2024 YiJiaLink, All Rights Reserved.
 * 2024-04-18 19:48:18
 */
#include "main.h"

/**
 * @addtogroup HC32F460_DDL_Examples
 * @{
 */

/**
 * @addtogroup GPIO_OUTPUT
 * @{
 */

/*******************************************************************************
 * Local type definitions ('typedef')
 ******************************************************************************/

/*******************************************************************************
 * Local pre-processor symbols/macros ('#define')
 ******************************************************************************/
/* LED_R Port/Pin definition */
#define LED_R_PORT (GPIO_PORT_B)
#define LED_R_PIN (GPIO_PIN_15)
/* LED_G Port/Pin definition */
#define LED_G_PORT (GPIO_PORT_B)
#define LED_G_PIN (GPIO_PIN_14)
/* LED_Y Port/Pin definition */
#define LED_Y_PORT (GPIO_PORT_B)
#define LED_Y_PIN (GPIO_PIN_13)
/* LED_B Port/Pin definition */
#define LED_B_PORT (GPIO_PORT_B)
#define LED_B_PIN (GPIO_PIN_12)
/* LED toggle definition */
#define LED_R_TOGGLE() (GPIO_TogglePins(LED_R_PORT, LED_R_PIN))
#define LED_G_TOGGLE() (GPIO_TogglePins(LED_G_PORT, LED_G_PIN))
#define LED_Y_TOGGLE() (GPIO_TogglePins(LED_Y_PORT, LED_Y_PIN))
#define LED_B_TOGGLE() (GPIO_TogglePins(LED_B_PORT, LED_B_PIN))

#define DLY_MS (1UL)

/*******************************************************************************
 * Global variable definitions (declared in header file with 'extern')
 ******************************************************************************/

/*******************************************************************************
 * Local function prototypes ('static')
 ******************************************************************************/
stc_clock_freq_t out_clk;
/*******************************************************************************
 * Local variable definitions ('static')
 ******************************************************************************/
/*******************************************************************************
 * Function implementation - global ('extern') and local ('static')
 ******************************************************************************/
/**
 * @brief  LED Init
 * @param  None
 * @retval None
 */
static void LED_Init(void)
{
  stc_gpio_init_t stcGpioInit;

  (void)GPIO_StructInit(&stcGpioInit);
  stcGpioInit.u16PinState = PIN_STAT_SET;
  stcGpioInit.u16PinDir = PIN_DIR_OUT;
  (void)GPIO_Init(LED_R_PORT, LED_R_PIN, &stcGpioInit);
  (void)GPIO_Init(LED_G_PORT, LED_G_PIN, &stcGpioInit);
  (void)GPIO_Init(LED_Y_PORT, LED_Y_PIN, &stcGpioInit);
  (void)GPIO_Init(LED_B_PORT, LED_B_PIN, &stcGpioInit);
}

/**
 * @brief  Main function of GPIO project
 * @param  None
 * @retval int32_t return value, if needed
 */
int main(void)
{
  LL_PERIPH_WE(LL_PERIPH_ALL);
  user_system_init();

  /* LED initialize */
  LED_Init();
  init_ws2812b();
  User_Timer_Init();

  /* Register write protected for some required peripherals. */
  LL_PERIPH_WP(LL_PERIPH_ALL);

  CLK_GetClockFreq(&out_clk);
  printf("u32SysclkFreq= %d \r\n", (int)out_clk.u32SysclkFreq);
  printf("u32HclkFreq= %d \r\n", (int)out_clk.u32HclkFreq);
  printf("SystemCoreClock= %d \r\n", (int)SystemCoreClock);
  init_rgb_pwm *p_pwm = &set_rgb_pwm;
  uint32_t data = 0xFF00FF;
  uint8_t i = 23;
  do{
  printf("data[%d] = %x\r\n",i,(int)(data & (1 << i)));
  }while(--i > 0);
	p_pwm->white_duty = PWM_CYCLE>>3;

  linklist_test_main();
  for (;;)
  {
  /* code */
  // rgb_math_run(p_pwm);
  run_ws2812b();
  DDL_DelayMS(DLY_MS);
  }
  return 0;
}

/**
 * @}
 */

/**
 * @}
 */

/*******************************************************************************
 * EOF (not truncated)
 ******************************************************************************/
