/*
 * @Author       : Deng
 * @Date         : 2024-04-18 22:28:14
 * @LastEditors  : Joel
 * @LastEditTime : 2024-05-17 10:32:55
 * @FilePath     : \src\usr_timer.c
 * @Description  :
 * Copyright 2024 YiJiaLink, All Rights Reserved.
 * 2024-04-18 22:28:14
 */
/* 包含头文件代码 */
#include "usr_timer.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

/* 私有(仅本源文件内使用)宏、枚举、结构体的定义 */

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

/* 内部函数(即 static 修饰)的声明 */

/*******************************************************************************
 * Variables
 ******************************************************************************/

/* 所有全局变量(外部，静态，常量，指针)的定义 */
init_rgb_pwm set_rgb_pwm = {.red_duty = 1};
init_rgb_pwm *p_pwm = &set_rgb_pwm;
/*******************************************************************************
 * Code
 ******************************************************************************/
/**
 * @brief  Configure TMR0.
 * @note   In asynchronous clock, If you want to write a TMR0 register, you need to wait for
 *         at least 3 asynchronous clock cycles after the last write operation!
 * @param  None
 * @retval None
 */
void TMR0_Config(void)
{
    stc_tmr0_init_t stcTmr0Init;
    stc_irq_signin_config_t stcIrqSignConfig;

    /* Enable timer0 and AOS clock */
    FCG_Fcg2PeriphClockCmd(TMR0_CLK, ENABLE);
    FCG_Fcg0PeriphClockCmd(FCG0_PERIPH_AOS, ENABLE);

    /* TIMER0 configuration */
    (void)TMR0_StructInit(&stcTmr0Init);
    stcTmr0Init.u32ClockSrc = TMR0_CLK_SRC_INTERN_CLK;      // 时钟源
    stcTmr0Init.u32ClockDiv = TMR0_CLK_DIV1;                // 分频系数100M/1 = 100Mhz
    stcTmr0Init.u32Func = TMR0_FUNC_CMP;                    // 计数模式
    stcTmr0Init.u16CompareValue = (uint16_t)TMR0_CMP_VALUE; // 计数值
    (void)TMR0_Init(TMR0_UNIT, TMR0_CH, &stcTmr0Init);
    TMR0_HWStopCondCmd(TMR0_UNIT, TMR0_CH, ENABLE);
    TMR0_IntCmd(TMR0_UNIT, TMR0_CH_INT, ENABLE);
    /* 外部中断触发 */
    // AOS_SetTriggerEventSrc(TMR0_TRIG_CH, EVT_SRC_PORT_EIRQ1);

    /* Interrupt configuration */
    stcIrqSignConfig.enIntSrc = TMR0_INT_SRC;
    stcIrqSignConfig.enIRQn = TMR0_IRQn;
    stcIrqSignConfig.pfnCallback = &User_Timer_IRQHandler;
    (void)INTC_IrqSignIn(&stcIrqSignConfig);
    NVIC_ClearPendingIRQ(stcIrqSignConfig.enIRQn);
    NVIC_SetPriority(stcIrqSignConfig.enIRQn, DDL_IRQ_PRIO_15);
    NVIC_EnableIRQ(stcIrqSignConfig.enIRQn);

    TMR0_Start(TMR0_UNIT, TMR0_CH);
    return;
}

/**
 * @brief  This function handles TIMERx interrupt request.
 * @param  None
 * @retval None
 */
void User_Timer_IRQHandler(void)
{


    /* 红色 */
    if (p_pwm->red_duty >= PWM_CYCLE)
    {
        (*(uint32_t *)RED_SET_PORT) = RED_GPIO_PIN;

        p_pwm->index_red = 0;
    }
    else if (p_pwm->red_duty == 0)
    {
        (*(uint32_t *)RED_RESET_PORT) = RED_GPIO_PIN;
        p_pwm->index_red = 0;
    }
    else if (++p_pwm->index_red >= PWM_CYCLE)
    {

        (*(uint32_t *)RED_SET_PORT) = RED_GPIO_PIN;
        p_pwm->index_red = 0;
    }
    else if (p_pwm->index_red >= p_pwm->red_duty)
    {
        (*(uint32_t *)RED_RESET_PORT) = RED_GPIO_PIN;
    }
    /* 红色 */
    if (p_pwm->green_duty >= PWM_CYCLE)
    {
        (*(uint32_t *)RED_SET_PORT) = GREEN_GPIO_PIN;

        p_pwm->index_green = 0;
    }
    else if (p_pwm->green_duty == 0)
    {
        (*(uint32_t *)RED_RESET_PORT) = GREEN_GPIO_PIN;
        p_pwm->index_green = 0;
    }
    else if (++p_pwm->index_green >= PWM_CYCLE)
    {

        (*(uint32_t *)RED_SET_PORT) = GREEN_GPIO_PIN;
        p_pwm->index_green = 0;
    }
    else if (p_pwm->index_green >= p_pwm->green_duty)
    {
        (*(uint32_t *)RED_RESET_PORT) = GREEN_GPIO_PIN;
    }
    /* 红色 */
    if (p_pwm->blue_duty >= PWM_CYCLE)
    {
        (*(uint32_t *)RED_SET_PORT) = BLUE_GPIO_PIN;

        p_pwm->index_blue = 0;
    }
    else if (p_pwm->blue_duty == 0)
    {
        (*(uint32_t *)RED_RESET_PORT) = BLUE_GPIO_PIN;
        p_pwm->index_blue = 0;
    }
    else if (++p_pwm->index_blue >= PWM_CYCLE)
    {

        (*(uint32_t *)RED_SET_PORT) = BLUE_GPIO_PIN;
        p_pwm->index_blue = 0;
    }
    else if (p_pwm->index_blue >= p_pwm->blue_duty)
    {
        (*(uint32_t *)RED_RESET_PORT) = BLUE_GPIO_PIN;
    }
     /* 红色 */
    if (p_pwm->white_duty >= PWM_CYCLE)
    {
        (*(uint32_t *)RED_SET_PORT) = WHITE_GPIO_PIN;

        p_pwm->index_white = 0;
    }
    else if (p_pwm->white_duty == 0)
    {
        (*(uint32_t *)RED_RESET_PORT) = WHITE_GPIO_PIN;
        p_pwm->index_white = 0;
    }
    else if (++p_pwm->index_white >= PWM_CYCLE)
    {

        (*(uint32_t *)RED_SET_PORT) = WHITE_GPIO_PIN;
        p_pwm->index_white = 0;
    }
    else if (p_pwm->index_white >= p_pwm->white_duty)
    {
        (*(uint32_t *)RED_RESET_PORT) = WHITE_GPIO_PIN;
    }
        /* here check timer interrupt bit */
    *(uint32_t *)PWM_TIMER_CLR = 0;
    return;
}

/**
 * @func:
 * @description: 定时器初始化
 * @return {*}
 * @example:
 */
void User_Timer_Init(void)
{

    TMR0_Config();
    return;
}

/* el psy congroo */
