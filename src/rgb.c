/*
 * rgb.c
 *
 *  Created on: 2024��5��6��
 *      Author: mkandy
 */

#include "rgb.h"

int run_rgbw(void)
{

    /* code */
            switch (p_pwm->brightness_index)
            {
            case RED /* constant-expression */:
                /* code */
                if (PWM_CYCLE == p_pwm->red_duty)
                    p_pwm->brightness_index++;
                else
                {
                    ++p_pwm->red_duty;
                    --p_pwm->blue_duty;

                }
                break;
            case GREEN /* constant-expression */:
                /* code */
                if (PWM_CYCLE == p_pwm->green_duty)
                    p_pwm->brightness_index++;
                else
                {
                    ++p_pwm->green_duty;
                    --p_pwm->red_duty;

                }

                break;
            case BLUE /* constant-expression */:
                /* code */
                if (PWM_CYCLE == p_pwm->blue_duty)
                    p_pwm->brightness_index = RED;
                else
                {
                    ++p_pwm->blue_duty;
                    --p_pwm->green_duty;

                }

                break;
            default:

                break;
            }
            DDL_DelayMS(2);

    return 0;
}
void rgb_math_run(init_rgb_pwm *p_pwm)
{
            if (++p_pwm->time % 128 == 0)
            {
                ++p_pwm->flag;
                // 正弦与余弦函数切换波形
                if (p_pwm->flag == 1)
                    p_pwm->data = !p_pwm->data;
                // 三色周期0-2
                else if (p_pwm->flag > 2)
                    p_pwm->flag = 0;
            }

            switch (p_pwm->flag)
            {
            case 0 /* constant-expression */:
                /* code */
                p_pwm->blue_duty = (uint16_t)get_apm_value(p_pwm->time, PWM_CYCLE,510, p_pwm->data);
                p_pwm->green_duty = (uint16_t)get_apm_value(p_pwm->time, PWM_CYCLE,510, !p_pwm->data);
                break;
            case 1 /* constant-expression */:
                /* code */
                p_pwm->red_duty = (uint16_t)get_apm_value(p_pwm->time, PWM_CYCLE,510, p_pwm->data);
                p_pwm->blue_duty = (uint16_t)get_apm_value(p_pwm->time, PWM_CYCLE,510, !p_pwm->data);
                break;
            case 2 /* constant-expression */:
                /* code */
                p_pwm->red_duty = (uint16_t)get_apm_value(p_pwm->time, PWM_CYCLE,510, p_pwm->data);
                p_pwm->green_duty = (uint16_t)get_apm_value(p_pwm->time, PWM_CYCLE,510, !p_pwm->data);
                break;
            default:
                break;
            }
        DDL_DelayMS(5);
        return;
}
float get_apm_value(uint16_t time, uint16_t a,uint16_t b,uint8_t flag)
{
    float index, value, data;

    index = (float)(2 * PI * time);
    value = (float)(index / b);
    if(flag)
    data = cosf(value);
    else
    data = sinf(value);
    /* 绝对值 */
    return (fabsf(a * data));
}